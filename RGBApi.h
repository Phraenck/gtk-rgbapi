#include <stdbool.h>

#ifndef RGBAPI_BASE_URL_LENGTH
#define RGBAPI_BASE_URL_LENGTH 80U
#endif

#ifndef RGBAPI_MAX_QUERY_LENGTH
#define RGBAPI_MAX_QUERY_LENGTH 20U
#endif

#ifndef RGBAPI_HEX_COLOR_LENGTH
#define RGBAPI_HEX_COLOR_LENGTH 6U
#endif

typedef enum {
  RGBAPISTATUS_SUCCESS,
  RGBAPISTATUS_FAIL,
  RGBAPISTATUS_NO_INIT,
  RGBAPISTATUS_BAD_ARGS
} RGBApiStatus;

typedef enum {
  RGBMODE_UNKNOWN,
  RGBMODE_SPECTRUM,
  RGBMODE_CONSTANT,
  RGBMODE_DUALFADE
} RGBMode;

void rgbapi_init(char* host, char* port);
void rgbapi_destroy();
RGBApiStatus rgbapi_rgbon();
RGBApiStatus rgbapi_rgboff();
RGBApiStatus rgbapi_constant(char* hexcolor);
RGBApiStatus rgbapi_dualfade(char* hexcolor1, char* hexcolor2);
RGBApiStatus rgbapi_dualfadesync();
RGBApiStatus rgbapi_query(RGBMode* mode, char* hexcolors);
bool rgbapi_isInitialized();
