#include <stdio.h>
#include <stdbool.h>
#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>

#include "RGBApi.h"

/**
 * Flag to say whether 'rgbapi_init' has been called.
 * Api functions will return RGBAPISTATUS_NO_INIT if they are called before 'rgbapi_init' has been called or if they have been called after
 * 'destroy' has been called.
 */
static bool initFlag_glbstat = false;

/**
 * private libcurl instance
 * Initialized by 'rgbapi_init'
 * Destroyed by 'destroy'
 */
static CURL* curlInst_glbstat;

/**
 * Base url to the server controlling the leds
 * e.g. "192.168.1.245:8181"
 */
static char baseurl_glbstat[RGBAPI_BASE_URL_LENGTH];

/**
 * geturl - append an additional string onto baseurl_glbstat and return it as a new string
 * inputs -
 * char* addnl - additional string to append onto baseurl_glbstat
 *
 * outputs
 * char*       - "baseurl_glbstat/addnl"; NOTE: this value must be freed by the caller!
 */
static char* geturl(char* addnl) {
  char* newstring = (char*)malloc(sizeof(char) * (strlen(baseurl_glbstat) + strlen(addnl) + 2U));
  sprintf(newstring, "%s/%s", baseurl_glbstat, addnl);

  return newstring;
}

/**
 * Trim the left side of a string and return the pointer to the first non-space charater
 * Inputs:
 * char* str - string to trim
 *
 * Return:
 * pointer to first non-space character in 'str'
 */
static char* strTrimLeft(char* str) {
  char* tmp = str;
  while(*tmp == ' ')
    tmp++;

  return tmp;
}

/**
 * Write callback for libcurl to be used by rgbapi_query
 */
static size_t curlWriteFunc(char* ptr, size_t size, size_t nmemb, char* userdata) {
  strncpy(userdata, ptr, nmemb);
  return size * nmemb;
}

/**
 * Parse the response from the RGB server to get the mode and current color
 * Inputs:
 * const char* rawResponse - raw response from the RGB Server (e.g. "SPECTRUM: 00FF00;")
 * 
 * Outputs:
 * RGBMode* mode           - current mode from the server
 * char* currHexColor      - current color being displayed
 */
static void parseQueryResp(const char* rawResponse, RGBMode* mode, char* currHexColor) {

  *mode = RGBMODE_UNKNOWN;

  if(rawResponse != NULL) {
    
    //store pointer in temporary variable since strtok will modify it
    char* rawResponseMod = (char*)rawResponse;
    char* token = strtok(rawResponseMod, ":");

    //determine the mode
    if(strcmp(token, "CONSTANT") == 0)
      *mode = RGBMODE_CONSTANT;
    else if(strcmp(token, "DUAL_FADE") == 0)
      *mode = RGBMODE_DUALFADE;
    else if(strcmp(token, "SPECTRUM") == 0)
      *mode = RGBMODE_SPECTRUM;

    //parse the color if the mode was determined
    if(*mode != RGBMODE_UNKNOWN) {
      token = strtok(NULL, ":");
      token = strTrimLeft(token);

      //copy 6 characters to currHexColor if token was parsed
      if(token != NULL && strlen(token) > 0) {
        strncpy(currHexColor, token, RGBAPI_HEX_COLOR_LENGTH);
      } else {
        *mode = RGBMODE_UNKNOWN;
      }
    }
  }
}

/**
 * Initialize RGBApi
 * Must be called before calling any of the other functions
 *
 * Inputs:
 * char* host - host address of the RGB server (e.g., "192.168.1.245")
 * char* port - port number that the RGB server is running on (e.g. "8181")
 */
void rgbapi_init(char* host, char* port) {
  sprintf(baseurl_glbstat, "%s:%s", host, port);

  curlInst_glbstat = curl_easy_init();
  initFlag_glbstat = true;
}

/**
 * Destroy RGBApi
 * Must be called before the program terminates
 */
void rgbapi_destroy() {
  curl_easy_cleanup(curlInst_glbstat);

  curlInst_glbstat = NULL;
  initFlag_glbstat = false;
}

/**
 * Instruct the RGB Server to perform the RGBON command
 *
 * Return:
 * RGBAPISTATUS_NO_INIT - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_SUCCESS - if server command was successful
 * RGBAPISTATUS_FAIL    - if server command was unsuccessful
 */
RGBApiStatus rgbapi_rgbon() {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  char url[RGBAPI_BASE_URL_LENGTH] = "lightstate/rgbon";
  char* fullurl = geturl(url);

  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  CURLcode res = curl_easy_perform(curlInst_glbstat);
  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}

/**
 * Instruct the RGB Server to perform the RGBOFF command
 *
 * Return:
 * RGBAPISTATUS_NO_INIT - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_SUCCESS - if server command was successful
 * RGBAPISTATUS_FAIL    - if server command was unsuccessful
 */
RGBApiStatus rgbapi_rgboff() {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  char url[RGBAPI_BASE_URL_LENGTH] = "lightstate/rgboff";
  char* fullurl = geturl(url);

  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  CURLcode res = curl_easy_perform(curlInst_glbstat);
  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}

/**
 * Instruct the RGB Server to perform the CONSTANT command
 *
 * Inputs:
 * char* hexcolor       - 6 character hexadecimal color to instruct the RGB server to use
 *
 * Return:
 * RGBAPISTATUS_NO_INIT  - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_BAD_ARGS - if hexcolor is not 6 charaters long
 * RGBAPISTATUS_SUCCESS  - if server command was successful
 * RGBAPISTATUS_FAIL     - if server command was unsuccessful
 */
RGBApiStatus rgbapi_constant(char* hexcolor) {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  if(strlen(hexcolor) != RGBAPI_HEX_COLOR_LENGTH)
    return RGBAPISTATUS_BAD_ARGS;

  char url[RGBAPI_BASE_URL_LENGTH] = "lightstate/constant/";
  strncat(url, hexcolor, RGBAPI_HEX_COLOR_LENGTH);

  char* fullurl = geturl(url);
  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  CURLcode res = curl_easy_perform(curlInst_glbstat);
  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}

/**
 * Instruct the RGB Server to perform the DUALFADE command
 *
 * Inputs:
 * char* hexcolor1      - 6 character hexadecimal color to instruct the RGB server to use
 * char* hexcolor2      - 6 character hexadecimal color to instruct the RGB server to use
 *
 * Return:
 * RGBAPISTATUS_NO_INIT  - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_BAD_ARGS - if hexcolor1 or hexcolor2 is not 6 charaters long
 * RGBAPISTATUS_SUCCESS  - if server command was successful
 * RGBAPISTATUS_FAIL     - if server command was unsuccessful
 */
RGBApiStatus rgbapi_dualfade(char* hexcolor1, char* hexcolor2) {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  if(strlen(hexcolor1) != RGBAPI_HEX_COLOR_LENGTH || strlen(hexcolor2) != RGBAPI_HEX_COLOR_LENGTH)
    return RGBAPISTATUS_BAD_ARGS;

  char url[RGBAPI_BASE_URL_LENGTH];
  sprintf(url, "lightstate/dualfade/%s%s", hexcolor1, hexcolor2);

  char* fullurl = geturl(url);
  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  CURLcode res = curl_easy_perform(curlInst_glbstat);

  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}



/**
 * Instruct the RGB Server to perform the DUALFADESYNC command
 *
 * Return:
 * RGBAPISTATUS_NO_INIT - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_SUCCESS - if server command was successful
 * RGBAPISTATUS_FAIL    - if server command was unsuccessful
 */
RGBApiStatus rgbapi_dualfadesync() {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  char url[RGBAPI_BASE_URL_LENGTH] = "lightstate/dualfadesync";
  char* fullurl = geturl(url);

  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  CURLcode res = curl_easy_perform(curlInst_glbstat);
  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}


/**
 * Query the mode and current color from the RGB Server
 *
 * Outputs:
 * RGBMode* mode      - current mode the RGB controller is in
 * char* currHexColor - current color the RGB controller is displaying
 *
 * Return:
 * RGBAPISTATUS_NO_INIT - if function is called before calling 'rgbapi_init' or after calling 'destroy'
 * RGBAPISTATUS_SUCCESS - if server command was successful
 * RGBAPISTATUS_FAIL    - if server command was unsuccessful
 */
RGBApiStatus rgbapi_query(RGBMode* mode, char* currHexColor) {
  if(!initFlag_glbstat)
    return RGBAPISTATUS_NO_INIT;

  char url[RGBAPI_BASE_URL_LENGTH] = "lightstate";
  char* fullurl = geturl(url);
  char apidata[RGBAPI_MAX_QUERY_LENGTH] = {0};

  //perform curl operation
  curl_easy_setopt(curlInst_glbstat, CURLOPT_URL, fullurl);
  curl_easy_setopt(curlInst_glbstat, CURLOPT_WRITEFUNCTION, curlWriteFunc);
  curl_easy_setopt(curlInst_glbstat, CURLOPT_WRITEDATA, apidata);
  CURLcode res = curl_easy_perform(curlInst_glbstat);

  //parse the response 
  parseQueryResp(apidata, mode, currHexColor);
  free(fullurl);

  return (res == CURLE_OK) ? RGBAPISTATUS_SUCCESS : RGBAPISTATUS_FAIL;
}

/**
 * Allow caller to check if api is initialized by returning initFlag_glbstat
 *
 * Return:
 * initFlag_glbstat
 */
bool rgbapi_isInitialized() {
  return initFlag_glbstat;
}
