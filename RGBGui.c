#include <gtk/gtk.h>
#include <stdlib.h>

#include "RGBApi.h"

#ifndef DEFAULT_MARGIN
#define DEFAULT_MARGIN 5U
#endif

#define ENVIRONMENT_VAR_NOT_FOUND_ERR "Environment variables RGBCONTROLPANEL_HOST and/or RGBCONTROLPANEL_PORT not set.\n"

typedef enum {
  DUALFADECOLOR_NONE = 0,
  DUALFADECOLOR_PRIMARY = 1,
  DUALFADECOLOR_SECONDARY = 2,
  DUALFADECOLOR_BOTH = 3
} DualFadeColor;

/**
 * Global static varible to inhibit timeout event once.
 * This is to ensure that the RGB Controller has finished a set operation before a query event takes place.
 * Will get set back to false after timeout handler executes once.
 */
static gboolean inhibitTimeoutOnce_glbstat = FALSE;

/**
 * Set inhibitTimeoutOnce_glbstat to TRUE
 */
static void inhibitTimeoutOnce() {
  inhibitTimeoutOnce_glbstat = TRUE;
}

/**
 * Convert a GdkRGBA color to a hexadecimal color string (ignore alpha channel)
 * Inputs:
 * GdkRGBA* color - pointer to a GdkRGBA structure
 *
 * Outputs:
 * char* - 7 character hexadecimal color string (includes null terminator)
 *
 * NOTE: char* return value must be freed by the caller
 */
static char* convGdkRGBAToHex(GdkRGBA* color) {
  char* dstBuf = (char*)malloc(sizeof(char) * 7U);

  unsigned int r = (unsigned int)(color->red * 255.0);
  unsigned int g = (unsigned int)(color->green * 255.0);
  unsigned int b = (unsigned int)(color->blue * 255.0);

  sprintf(dstBuf, "%02X%02X%02X", r, g, b);

  return dstBuf;
}

/**
 * Convert a hexadecimal color string to a GdkRGBA structure (set alpha channel to 1.0)
 * Inputs:
 * const char* hexcolor - hexadecimal string to convert
 *
 * Outputs:
 * GdkRGBA* - RdkRGBA structure representing the same color as hexcolor
 *
 * NOTE: GdkRGBA* return value must be freed by the caller
 */
static GdkRGBA* convHextoGdkRGBA(const char* hexcolor) {
  GdkRGBA* dstStruct;

  //return NULL if hexcolor isn't an appropriate length
  if(strlen(hexcolor) == RGBAPI_HEX_COLOR_LENGTH)
    dstStruct = (GdkRGBA*)malloc(sizeof(GdkRGBA));
  else {
    g_warning("Invalid hexcolor: \"%s\"", hexcolor);
    return NULL;
  }

  //copy one color channel at a time to tmpBuf, convert it from char to long, and convert to a percentage
  char tmpBuf[3] = {0};

  //red
  strncpy(tmpBuf, &hexcolor[0], 2U); //[rr]ggbb
  dstStruct->red = strtol(tmpBuf, NULL, 16) / 255.0;

  //green
  strncpy(tmpBuf, &hexcolor[2], 2U); //rr[gg]bb
  dstStruct->green = strtol(tmpBuf, NULL, 16) / 255.0;

  //blue
  strncpy(tmpBuf, &hexcolor[4], 2U); //rrgg[bb]
  dstStruct->blue = strtol(tmpBuf, NULL, 16) / 255.0;

  dstStruct->alpha = 1.0;

  return dstStruct;
}

/**
 * Perform rgbapi_query operation and display in a GtkColorChooser
 *
 * Inputs:
 * GtkColorChooser* dstColorChooser - GtkColorChooser to display a color in
 */
static void queryAndDisplay(GtkColorChooser* dstColorChooser) {
  RGBMode mode;
  char hexcolor[RGBAPI_HEX_COLOR_LENGTH + 1] = {0};
  RGBApiStatus sts = rgbapi_query(&mode, hexcolor);
  g_print("%i\n", sts);

  GdkRGBA* color = convHextoGdkRGBA(hexcolor);
  if(color != NULL) {
    gtk_color_chooser_set_rgba(dstColorChooser, color);
    free(color);
  }
}

/**
 * Handler for g_timeout_add_seconds.
 * Queries the RGB Server periodically and displays the current color
 *
 * Inputs:
 * gpointer userData - GtkColorChooser to display a color in
 *
 * Outputs:
 * gboolean - always TRUE so the timer event continues to run
 */
static gboolean queryRefreshPeriodic_handler(gpointer userData) {
  if(inhibitTimeoutOnce_glbstat == FALSE)
    queryAndDisplay((GtkColorChooser*) userData);

  inhibitTimeoutOnce_glbstat = FALSE;
  return TRUE;
}

/**
 * Click event handler for btnRgbOn
 * Execute rgbapi_rgbon and print status
 *
 * Inputs:
 * GtkWidget* app - unused
 * gpointer data  - unused
 */
static void btnRgbOn_handler(GtkWidget* app, gpointer data) {
  inhibitTimeoutOnce();
  RGBApiStatus sts = rgbapi_rgbon();
  g_print("%i\n", sts);
}

/**
 * Click event handler for btnRgbOn
 * Execute rgbapi_rgboff and print status
 *
 * Inputs:
 * GtkWidget* app - unused
 * gpointer data  - unused
 */
static void btnRgbOff_handler(GtkWidget* app, gpointer data) {
  inhibitTimeoutOnce();
  RGBApiStatus sts = rgbapi_rgboff();
  g_print("%i\n", sts);
}

/**
 * Click event handler for btnDualFadeSync
 * Execute rgbapi_dualfadesync and print status
 *
 * Inputs:
 * GtkWidget* app - unused
 * gpointer data  - unused
 */
static void btnDualFadeSync_handler(GtkWidget* app, gpointer data) {
  inhibitTimeoutOnce();
  RGBApiStatus sts = rgbapi_dualfadesync();
  g_print("%i\n", sts);
}

/**
 * Click event handler for btnQuery
 * Execute rgbapi_query and print status
 *
 * Inputs:
 * GtkWidget* app - unused
 * gpointer data  - GtkColorChooser* to display the color from the RGB server in
 */
static void btnQuery_handler(GtkWidget* app, gpointer data) {
  inhibitTimeoutOnce();
  queryAndDisplay((GtkColorChooser*)data);
}

/**
 * Click event handler for btnConstColor
 * Execute rgbapi_constant using the selected color from GtkColorButton and print status
 *
 * Inputs:
 * GtkColorButton* self - GtkColorButton to get color from and pass to api
 * gpointer user_data   - unused
 */
static void btnConstColor_color_set_handler(GtkColorButton* self, gpointer user_data) {
  inhibitTimeoutOnce();

  GdkRGBA color;
  gtk_color_chooser_get_rgba((GtkColorChooser*)self, &color);
  char* hex = convGdkRGBAToHex(&color);
  RGBApiStatus sts = rgbapi_constant(hex);
  g_print("%i\n", sts);

  free(hex);
  hex = NULL;
}

/**
 * Click event handler for btnDualFadeColor1 and btnDualFadeColor2
 * Execute rgbapi_dualfade once both buttons have selected a color
 *
 * Inputs:
 * GtkColorButton* self - GtkColorButton to get primary or secondary color from
 * gpointer user_data   - either DUALFADECOLOR_PRIMARY or DUALFADECOLOR_SECONDARY.  Used to identify whether the signal is comming from
 *                        button 1 or 2
 */
static void btnDualFadeColor_color_set_handler(GtkColorButton* self, gpointer user_data) {
  //static variables to track colors and status
  static GdkRGBA primaryColor;
  static GdkRGBA secondaryColor;
  static DualFadeColor colorsSet = DUALFADECOLOR_NONE;

  colorsSet |= (DualFadeColor)user_data;

  //set primaryColor or secondaryColor depending on the signal source
  switch((DualFadeColor)user_data) {
    case DUALFADECOLOR_PRIMARY:
      gtk_color_chooser_get_rgba((GtkColorChooser*)self, &primaryColor);
      break;
    case DUALFADECOLOR_SECONDARY:
      gtk_color_chooser_get_rgba((GtkColorChooser*)self, &secondaryColor);
      break;
    default:
      //should never happen; reset just to be safe
      g_warning("btnDualFadeColor_color_set_handler: invalid user_data");
      colorsSet = DUALFADECOLOR_NONE;
      break;
  }

  //perform api operation once both colors are set
  if(colorsSet == DUALFADECOLOR_BOTH) {
    inhibitTimeoutOnce();
    char* hexcolor1 = convGdkRGBAToHex(&primaryColor);
    char* hexcolor2 = convGdkRGBAToHex(&secondaryColor);
    RGBApiStatus sts = rgbapi_dualfade(hexcolor1, hexcolor2);
    g_print("%i\n", sts);
    free(hexcolor1);
    free(hexcolor2);

    colorsSet = DUALFADECOLOR_NONE;
  }
}

/**
 * Quit the application when the user clicks the close button on dlgEnvVarNotFound
 */
static void dlgEnvVarNotFound_handler(GtkWidget* app, gpointer data) {
  g_application_quit(G_APPLICATION(data));
}

/**
 * Build the UI
 */
static void activate(GtkApplication* app, gpointer user_data) {
  GtkWidget* window = gtk_application_window_new(app);

  gtk_window_set_title(GTK_WINDOW(window), "RGB Gui");
  gtk_window_set_default_size(GTK_WINDOW (window), 400, 100);
  gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

  //BOX
  GtkWidget* gridMain = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(gridMain), DEFAULT_MARGIN);
  gtk_grid_set_column_spacing(GTK_GRID(gridMain), DEFAULT_MARGIN);
  gtk_grid_set_column_homogeneous(GTK_GRID(gridMain), TRUE);
  gtk_widget_set_margin_end(gridMain, DEFAULT_MARGIN);
  gtk_widget_set_margin_start(gridMain, DEFAULT_MARGIN);
  gtk_widget_set_margin_top(gridMain, DEFAULT_MARGIN);
  gtk_widget_set_margin_bottom(gridMain, DEFAULT_MARGIN);

  gtk_window_set_child(GTK_WINDOW(window), gridMain);

  //RGB ON
  GtkWidget* btnRgbOn = gtk_button_new_with_label("RGB On");
  g_signal_connect(btnRgbOn, "clicked", G_CALLBACK(btnRgbOn_handler), NULL);

  gtk_grid_attach(GTK_GRID(gridMain), btnRgbOn, 0, 0, 1, 1);


  //RGB OFF
  GtkWidget* btnRgbOff = gtk_button_new_with_label("RGB Off");
  g_signal_connect(btnRgbOff, "clicked", G_CALLBACK(btnRgbOff_handler), NULL);

  gtk_grid_attach(GTK_GRID(gridMain), btnRgbOff, 1, 0, 1, 1);


  //DUAL FADE SYNC
  GtkWidget* btnDualFadeSync = gtk_button_new_with_label("Dual Fade Sync");
  g_signal_connect(btnDualFadeSync, "clicked", G_CALLBACK(btnDualFadeSync_handler), NULL);

  gtk_grid_attach(GTK_GRID(gridMain), btnDualFadeSync, 0, 1, 1, 1);


  //QUERY
  GtkWidget* boxQuery = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, DEFAULT_MARGIN);

  gtk_box_set_homogeneous(GTK_BOX(boxQuery), TRUE);
  GtkWidget* btnQuery = gtk_button_new_with_label("Query");
  GtkWidget* btnQueryColor = gtk_color_button_new();
  gtk_widget_set_sensitive(btnQueryColor, FALSE);

  g_signal_connect(btnQuery, "clicked", G_CALLBACK(btnQuery_handler), (gpointer)btnQueryColor);

  gtk_box_append(GTK_BOX(boxQuery), btnQuery);
  gtk_box_append(GTK_BOX(boxQuery), btnQueryColor);

  gtk_grid_attach(GTK_GRID(gridMain), boxQuery, 1, 1, 1, 1);


  //RGB CONST
  GtkWidget* lblConst = gtk_label_new("Constant");
  GtkWidget* sepMainConst = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);

  GtkWidget* btnConstColor = gtk_color_button_new();
  g_signal_connect(btnConstColor, "color-set", G_CALLBACK(btnConstColor_color_set_handler), NULL);

  gtk_grid_attach(GTK_GRID(gridMain), sepMainConst, 0, 2, 2, 1);
  gtk_grid_attach(GTK_GRID(gridMain), lblConst, 0, 3, 1, 1);
  gtk_grid_attach(GTK_GRID(gridMain), btnConstColor, 1, 3, 1, 1);


  //DUAL FADE  
  GtkWidget* sepConstDualFade = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
  GtkWidget* lblDualFade = gtk_label_new("Dual Fade");
  GtkWidget* btnDualFadeColor1 = gtk_color_button_new();
  GtkWidget* btnDualFadeColor2 = gtk_color_button_new();
  g_signal_connect(btnDualFadeColor1, "color-set", G_CALLBACK(btnDualFadeColor_color_set_handler), (gpointer)DUALFADECOLOR_PRIMARY);
  g_signal_connect(btnDualFadeColor2, "color-set", G_CALLBACK(btnDualFadeColor_color_set_handler), (gpointer)DUALFADECOLOR_SECONDARY);

  gtk_grid_attach(GTK_GRID(gridMain), sepConstDualFade, 0, 4, 2, 1);
  gtk_grid_attach(GTK_GRID(gridMain), lblDualFade, 0, 5, 1, 2);
  gtk_grid_attach(GTK_GRID(gridMain), btnDualFadeColor1, 1, 5, 1, 1);
  gtk_grid_attach(GTK_GRID(gridMain), btnDualFadeColor2, 1, 6, 1, 1);

  //SHOW WINDOW
  gtk_widget_show(window);

  //if the rgbapi is not initialized, display a dialog, and close the application when the user clicks 'close'
  if(rgbapi_isInitialized()) {
    //Query the current color periodically and update btnQueryColor
    queryAndDisplay((GtkColorChooser*)btnQueryColor);
    g_timeout_add_seconds(2U, queryRefreshPeriodic_handler, (gpointer)btnQueryColor);
  } else {
    gtk_widget_set_sensitive(window, FALSE);

    GtkWidget* dlgEnvVarNotFound = gtk_message_dialog_new(GTK_WINDOW(window),
        GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_CLOSE,
        ENVIRONMENT_VAR_NOT_FOUND_ERR);

    g_signal_connect(dlgEnvVarNotFound, "response", G_CALLBACK(dlgEnvVarNotFound_handler), (gpointer)app);

    gtk_widget_show(dlgEnvVarNotFound);
  }
}

#if !defined(API_DEBUG_ENABLED) || API_DEBUG_ENABLED == 0
/**
 * Print the command line arguments if they were invalid
 *
 * Inputs:
 * char** values    - array of strings to print
 * unsigned int num - number of strings in 'values'
 */
static void invalidCmdArgs(char** values, unsigned int num) {
  printf("Unexpected command line argument(s): ");
  for(unsigned int i = 0; i < num; i++) {
    printf("%s ", values[i]);
  }
}

/**
 * Main entry point
 */
int main (int argc, char** argv) {

  char* host = getenv("RGBCONTROLPANEL_HOST");
  char* port = getenv("RGBCONTROLPANEL_PORT");

  //check that the environment variables exist and initialize the api if they do
  if(host != NULL && port != NULL) {
    rgbapi_init(host, port);
  } else {
    fprintf(stderr, ENVIRONMENT_VAR_NOT_FOUND_ERR);
  }

  RGBApiStatus rgbapiSts = RGBAPISTATUS_BAD_ARGS;
  int status = 0;

  switch(argc) {
    case 1:
      //display GTK gui if no arguments were passed
      GtkApplication *app = gtk_application_new("org.gtk.rgbgui", G_APPLICATION_FLAGS_NONE);

      g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

      status = g_application_run(G_APPLICATION(app), 1, &argv[0]);
      g_object_unref(app);

      break;
    case 2:
      //handle api function that don't require any arguments (rgbon, rgboff, dualfadesync)
      if(strcmp(argv[1], "rgbon") == 0)
        rgbapiSts = rgbapi_rgbon();
      else if(strcmp(argv[1], "rgboff") == 0)
        rgbapiSts = rgbapi_rgboff();
      else if(strcmp(argv[1], "dualfadesync") == 0)
        rgbapiSts = rgbapi_dualfadesync();
      else if(strcmp(argv[1], "query") == 0) {
        RGBMode mode;
        char color[RGBAPI_HEX_COLOR_LENGTH + 1] = {0};
        rgbapiSts = rgbapi_query(&mode, color);
        printf("%i\t%s\n", mode, color);
      }

      //display bad arguments
      if (rgbapiSts == RGBAPISTATUS_BAD_ARGS) {
        invalidCmdArgs(&argv[1], 1U);
        status = 1;
      }

      break;
    case 3:
      //handle api functions that require only one argument (constant)
      if(strcmp(argv[1], "constant") == 0)
        rgbapiSts = rgbapi_constant(argv[2]);

      //display bad arguments
      if(rgbapiSts == RGBAPISTATUS_BAD_ARGS) {
        invalidCmdArgs(&argv[1], 2U);
        status = 1;
      }

      break;
    case 4:
      //handle api functions that require 2 arguments (dualfade)
      if(strcmp(argv[1], "dualfade") == 0)
        rgbapiSts = rgbapi_dualfade(argv[2], argv[3]);

      //display bad arguments
      if(rgbapiSts == RGBAPISTATUS_BAD_ARGS) {
        invalidCmdArgs(&argv[1], 3U);
        status = 1;
      }

      break;
    default:
      printf("Invalid number of arguments\n");
      if(argc > 1)
        invalidCmdArgs(&argv[1], argc - 1U);

      status = 1;
  }

  rgbapi_destroy();
  return status;
}
#endif
