#include <stdio.h>
#include <unistd.h>

#include "RGBApi.h"

#if defined(API_DEBUG_ENABLED) && API_DEBUG_ENABLED != 0

int main(int argc, char** argv) {
  rgbapi_init("192.168.1.245", "8181");

  rgbapi_constant("0000FF");
  sleep(2);
  RGBMode mode;

  char color[RGBAPI_HEX_COLOR_LENGTH + 1] = {0};
  rgbapi_query(&mode, color);

  printf("%i\t%s\n", mode, color);

  //printf("dualfade\n");
  //rgbapi_dualfade("FF0000","0000FF");
  //sleep(3);

  //printf("constant\n");
  //rgbapi_constant("00FF00");
  //sleep(3);

  //printf("rgboff\n");
  //rgbapi_rgboff();
  //sleep(3);

  //printf("rgbon\n");
  //rgbapi_rgbon();
  rgbapi_destroy();

  return 0;
}
#endif
