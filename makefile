CC=gcc
SRC=RGBGui.c RGBApi.c Test.c
OUT=RGBControlPanel
LIBS=-Llibs -lcurl $(shell pkg-config --libs gtk4)
CFLAGS=-Wall -g $(shell pkg-config --cflags gtk4)

build:
	$(CC) $(CFLAGS) $(SRC) -o $(OUT) $(LIBS)

build_debug:
	$(CC) $(CFLAGS) $(SRC) -o $(OUT) $(LIBS) -DAPI_DEBUG_ENABLED=1

run:
	./$(OUT)

clean:
	rm *.o
